package coinpurse.strategy;

import java.util.ArrayList;

import java.util.List;

import coinpurses.Valuable;
/**
 * Interface that use for withdraw the value from purse by using recursive way.
 * @author Natcha Pongsupanee 5710546224
 */
public class RecursiveWithdraw implements WithdrawStrategy<Valuable> {
	/**
	 * Getting the list of value that we have to with draw by using recurtion
	 * @return List of money we with draw.
	 */
	public List<Valuable> withdraw(double amount,List<Valuable> valuables){

		List<Valuable> returnValue = withdrawFrom(amount,valuables,valuables.size()-1);
		if(returnValue==null){
			return withdrawFrom(amount,valuables.subList(0, valuables.size()-1),valuables.size()-2);	
		}
		return returnValue;
	}
	/**
	 * this method is helper of withdraw()
	 * @return List of money we with draw.
	 */
	public List<Valuable> withdrawFrom(double amount,List<Valuable> valuables,int lastIndex){
		if(lastIndex<0&&amount!=0){
			return null;
		}
		if(amount<0){
			return null;
		}
		if(amount-valuables.get(lastIndex).getValue()==0){
			List<Valuable> returnValue = new ArrayList<Valuable>();
			returnValue.add(valuables.get(lastIndex));
			return returnValue;
		}
		List<Valuable> returnValue = withdrawFrom(amount-valuables.get(lastIndex).getValue(),valuables,lastIndex-1);
		if(returnValue == null){
			return withdrawFrom(amount,valuables,lastIndex-1);
		}
		else{
			returnValue.add(valuables.get(lastIndex));
		}
		return returnValue;
		
	}
}
