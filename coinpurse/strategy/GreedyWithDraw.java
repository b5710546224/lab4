package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurses.Valuable;
/**
 * Interface that use for withdraw the value from purse by using greedy way.
 * @author Natcha Pongsupanee 5710546224
 */
public class GreedyWithDraw implements WithdrawStrategy<Valuable>{
	/**
	 * method for Getting the list of value by using greedy withdraw
	 * @return List of money we with draw.
	 */
	public List<Valuable> withdraw(double amount,List<Valuable> valuables){
        double test = 0;
      List<Valuable> fakePurse = new ArrayList<Valuable>();
      fakePurse.addAll(valuables);
        List<Valuable> moneyPicked = new ArrayList<Valuable>();
        for(int a = fakePurse.size()-1;a>=0;a--){
        	test += fakePurse.get(a).getValue();
        	moneyPicked.add(fakePurse.get(a));
        	valuables.remove(a);
        	if(test > amount){
        		test -= fakePurse.get(a).getValue();
        		moneyPicked.remove(moneyPicked.size()-1);
            	valuables.add(fakePurse.get(a));
        	}
        }
        if(test!=amount)
        	return null;
        return moneyPicked;
	}
}
