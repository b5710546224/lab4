package coinpurse.strategy;
import java.util.List;

import coinpurses.Valuable;
/**
 * Interface that use for withdraw the value from purse.
 * @author Natcha Pongsupanee 5710546224
 */
public interface WithdrawStrategy<Valuable> {
	/**
	 * Getting the list of value
	 * @return List of money we with draw.
	 */
	public List<Valuable> withdraw(double amount,List<Valuable> valuables);
}
