package coinpurses;

public class MalaiMoneyFactory extends MoneyFactory{
	@Override
	Valuable createMoney(double value) {
		int caseV = (int)value;
		if(value == 0.05){
			return new Coin(5,"sen");
		}else if(value == 0.01){
			return new Coin(1,"sen");
		}else if(value == 0.20){
			return new Coin(20,"sen");
		}else if(value == 0.50){
			return new Coin(50,"sen");
		}else{
			switch (caseV){
		    	case 20: case 50: case 100: case 500: case 1000:
		    		return new BankNote(value,"Ringgit");
		    	default:
		    		return null;
			}
		}
	}

}
