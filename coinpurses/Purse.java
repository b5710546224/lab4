package coinpurses;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import coinpurse.strategy.GreedyWithDraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A purse contains coins,coupon and banknote.
 *  You can insert coins,coupon or banknote, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Natcha Pongsupanee 5710546224
 */
public class Purse extends Observable{
    /** Collection of coins in the purse. */
	
    private ArrayList<Valuable> moneyList;
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    private final ValueComparator comparator = new ValueComparator();
    private Valuable valuables;
    private WithdrawStrategy strategy;

    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	moneyList = new ArrayList<Valuable>();
    }
    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { 
    	int count = 0;
    	for(int a = 0;a<moneyList.size();a++){
    		count++;
    	}
    	return count; 
    }
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance = 0;
    	for(int a = 0;a<moneyList.size();a++){
    		balance += moneyList.get(a).getValue();
    	}
    	return balance; 
    }
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {	
       if(this.capacity == moneyList.size()){
    	   	super.setChanged();
   			super.notifyObservers(this);
        return true;
       }
       else{
       return false;
       }
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param coin is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Valuable money ) {
    	if(money.getValue()<=0){
    		return false;
    	}
    	if(isFull()){
    		return false;
    	}
    	else{
    		moneyList.add(money);
    		super.setChanged();
    		super.notifyObservers(this);
    		return true;
    	}
   
    }
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw(double amount) {
    	if(amount <=0){
    		return null;
    	}
        if (this.getBalance()<amount){
        	return null;
        }
        Collections.sort(moneyList,comparator);
       strategy = new GreedyWithDraw();
       	ArrayList<Valuable> moneyList2 = (ArrayList<Valuable>) moneyList.clone();
        List<Valuable> moneyPicked = this.strategy.withdraw(amount, moneyList2);
        if(moneyPicked == null){
 
        	strategy = new RecursiveWithdraw();
        	moneyPicked = this.strategy.withdraw(amount,moneyList);
        }
        if(moneyPicked == null){return null;}
        else{moneyList.removeAll(moneyPicked);}
        Valuable[] reMoneyList = new Valuable[moneyPicked.size()];
        for(int b = 0;b<moneyPicked.size();b++){
        	reMoneyList[b] = moneyPicked.get(b);
        }
        if(reMoneyList.length==0){
        	return null;
        }
        else{
        	double test2 = 0;
        	for(int v = 0;v<reMoneyList.length;v++){
        		test2 += reMoneyList[v].getValue();
        	}
        	if(amount-test2 == 0){
        		super.setChanged();
        		super.notifyObservers(this);
        		return reMoneyList;
        	}
        	else{
        		for(int q = 0;q<reMoneyList.length;q++){
        			moneyList.add(reMoneyList[q]);
        		}
        		return null;
        	}
        	
        }
	}
    public void setWithdrawStrategy(WithdrawStrategy newStrategy){
    	this.strategy = newStrategy;
    }
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
        String str =String.format("%d coins with value %.1f", this.count(),this.getBalance());
    	return str;
    }

}