package coinpurses;
/**
 * Interface that use for getting the value of Object.
 * @author Natcha Pongsupanee 5710546224
 */
public interface Valuable extends Comparable<Valuable> {
	/**
	 * Getting the value of Object.
	 * @return the value of Object
	 */
	public double getValue();
}
