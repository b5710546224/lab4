package coinpurses;

import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;

/**
 *  The Class that control GUI and Update every time we do work
 *  with purse
 *  @author Natcha Pongsupanee 5710546224
 */
public class PurseObserver extends JFrame implements Observer{
	/** JLable that used to explain how the purse going on */
	private JLabel balance,space;
	/**Tab JProgressBar explain space in our Purse*/
	private JProgressBar progressBar;
	/**variable decare our purse */
	Purse purse;
	/** 
     *  Constucter for a new PurseObserver
     *  @param purse that is sent from main.
     */
	public PurseObserver(Purse purse){
		super("Coin Purse");
		this.purse = purse;
		this.initComponets();
	}
	/**
	 * update receives notification from the purse
	 **/
	public void update(Observable subject,Object info){
		System.out.println("EIEI");
		if(subject instanceof Purse){
			Purse purse = (Purse) subject;
			balance.setText("Balance : "+purse.getBalance() + " Baht");
			if(purse.count()==purse.getCapacity())
				space.setText("Purse Full!!");
			
			else space.setText(purse.getCapacity()-purse.count()+" Space left");
			progressBar.setValue(purse.count());
		}
		if(info != null){
			System.out.println(info);
		}
	}
	/** 
     *  GUI Structer
     */
	private void initComponets(){
		JFrame frame = new JFrame();
		JPanel pane = new JPanel();
		JLabel padder = new JLabel(" ");
		Font padFont = padder.getFont();
		padder.setFont((new Font(padFont.getName(),Font.PLAIN,15)));
		JLabel padder1 = new JLabel(" ");
		Font padFont1 = padder.getFont();
		padder1.setFont((new Font(padFont1.getName(),Font.PLAIN,15)));
		balance = new JLabel("Balance : "+purse.getBalance()+ " Baht");
		Font font = balance.getFont();
		balance.setFont(new Font(font.getName(),Font.PLAIN,25));
		
		space = new JLabel(purse.getCapacity()-purse.count()+" Space left");
		Font fontSpace = space.getFont();
		space.setFont(new Font(fontSpace.getName(),Font.PLAIN,25));
		space.setSize(20, 10);
		progressBar = new JProgressBar(0,purse.getCapacity());
		progressBar.setSize(20,10);
		balance.setAlignmentX(CENTER_ALIGNMENT);
		space.setAlignmentX(CENTER_ALIGNMENT);
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		pane.setSize(20, 4);
		pane.add(balance);
		pane.add(padder);
		pane.add(space);
		pane.add(padder1);
		pane.add(progressBar);
		frame.add(pane);
		frame.pack();
		frame.setVisible(true);
	}
}
