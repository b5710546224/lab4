package coinpurses;

public abstract class AbstractValuable implements Valuable {
	double value;
	String currency;
	
	public AbstractValuable(double value,String currency){
		this.value = value;
		this.currency = currency;
	}
	
    /** 
     * method for checking if the other in parameter less than, more than or equal value.
     * @param other is Valuable for compare with.
     * @return -1 if other more than this.
     * 			1 if other less than this.
     * 			0 if other and this is the same value.
     */
	public int compareTo(Valuable other){
		if(other.equals(null)){
    		return 1;
    	}
    	if(this.getValue() < other.getValue()){
    		return -1;
    	}
    	else if(this.getValue() > other.getValue()){
    		return 1;
    	} 
    	else{
    		return 0;
    	}
	}
	/** 
     * method for checking if the Object in parameter has the same value. 
     * @param obj is the Object that we want to check with
     * @return false if obj is null
     * 		   false if obj is a different object with this
     * 		   ture is the value of this and obj is the same
     */
	public boolean equals(Object obj){
		 if (obj == null) return false;
	     if ( obj.getClass() != this.getClass() )
	          return false;
	     Valuable other = (Valuable) obj;
	     if ( this.getValue()==other.getValue() )
	          return true;
	     return false;
	}
}
