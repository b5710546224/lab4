package coinpurses;

public class ThaiMoneyFactory extends MoneyFactory{
	private String currency = "Bath";
	@Override
	Valuable createMoney(double value) {
		 int caseV = (int)value;
		switch (caseV){
		     case 1: case 2: case 5: case 10:
		    	return new Coin(value,currency);
		     case 20: case 50: case 100: case 1000:
		    	return new BankNote(value,currency);
		     default:
		    	return null;
		}
	}

}
