package coinpurses;
import java.util.Comparator;
/**
 * @author Natcha Pongsupanee 5710546224
 */
public class ValueComparator implements Comparator<Valuable> {
/**
 * method to compare between a and b
 * @param a and b is the variable to compare
 */
	public int compare(Valuable a, Valuable b) {
		if(a.equals(null)||b.equals(null)){
    		return 1;
    	}
    	if(a.getValue() < b.getValue()){
    		return -1;
    	}
    	else if(a.getValue()>b.getValue()){
    		return 1;
    	} 
    	else{
    		return 0;
    	}
	}
}