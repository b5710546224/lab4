package coinpurses;

import coinpurse.strategy.GreedyWithDraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    public static void main( String[] args ) {
    	MoneyFactory factory = MoneyFactory.getInstance();
    	Valuable money = factory.createMoney("0.05");
    	System.out.print(money);
    }
}