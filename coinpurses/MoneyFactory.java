package coinpurses;

import java.util.ResourceBundle;

public abstract class MoneyFactory {
	private static MoneyFactory instance = null;
	public static MoneyFactory getInstance(){
		ResourceBundle bundle = ResourceBundle.getBundle("map");
		String value = bundle.getString("default");
		if(instance==null){
			try {
				instance = (MoneyFactory)Class.forName(value).newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}
	abstract Valuable createMoney(double value);
	public Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));
	}
	
}
