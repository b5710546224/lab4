package coinpurses;
import java.util.HashMap;
import java.util.Map;
/**
* A Coupon with a monetary value.
* You can't change the value of a Coupon.
* @author Natcha Pongsupanee 5710546224
*/
public class Coupon {
	 /** The color of the coupon */
	private String color;
	 /** This use to store the monetary value of each coupon color */
	private Map<String,Double> map;
	/** 
     * Constructor for a new Coupon. 
     * @param The color of coupon
     */
	public Coupon(String color){
		map = new HashMap<String,Double>( ); 
		map.put( "red", 100.0 );
		map.put( "blue", 50.0 ); 
		map.put( "green", 20.0 );
		this.color = color.toLowerCase();
		if(!map.containsKey(this.color)){
			this.color = null;
		}
	}
	/** 
     * method for getting the value of Coupon. 
     * @return the value of coupon depend on the color
     */
	public double getValue(){
		return map.get(this.color);
	}
	/** 
	 * toString returns a string description of the color of coupon.
     * It can return whatever is a useful description.
     */
	public String toString(){
		return String.format("%s Coupon",this.color);
	}
}