package coinpurses;
 /**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Natcha Pongsupanee
 */
public class Coin extends AbstractValuable{
    /** Value of the coin */
    private double x;
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ,String currency) {
    	super(value,currency);
    }
    /** 
     * method for getting the value of coin. 
     * @return the value
     */
    public double getValue(){
    	return this.value;
    }
    /** 
	 * toString returns a string description of the value of Coin.
     * It can return whatever is a useful description.
     */
    public String toString(){
    	return String.format("%.0f-%s coin",this.value,this.currency);
    }
}