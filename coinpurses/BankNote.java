package coinpurses;

/** 
 * This is a class for banknote that have one static variable is nextSerialNumber
 * used to set a serial number of each banknote that will begin with 1000000
 *  @author Natcha Pongsupanee 5710546224
 */
public class BankNote extends AbstractValuable{
	 /** Value of the banknote */
    private double value;
    private static long nextSerialNumber = 999999;
    private long serialNumber;
    /** 
     * Constructor for a new banknote. 
     * @param value is the value for the banknote
     * 		  serialNumber is the serial number of the banknote
     */
	public BankNote(double value,String currency){
		super(value,currency);
		this.serialNumber = serialNumber;
		serialNumber = getNextSerialNumber();
		
	}
	/** 
     * method for getting the value of BankNote. 
     * @return the value of this banknote
     */
	public double getValue(){
		return this.value;
	}
	/** 
     * @return the updated value of nextSerialNumber
     */
	public long getNextSerialNumber(){
		return ++this.nextSerialNumber;
	}
	/** 
	 * toString returns a string description of the value of banknote.
     * It can return whatever is a useful description.
     */
	public String toString(){
		return String.format("%.0f-%s BankNote[%d]",this.value,this.currency,this.serialNumber);
	}
	
}